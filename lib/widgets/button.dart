import 'package:flutter/material.dart';
import 'package:la_chacha_app/app_properties.dart';

class Button extends StatelessWidget {
  final Color color;
  final String title;
  final VoidCallback onTap;
  const Button({
    Key key,
    this.color = buttonPrimary,
    this.title,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 50.0,
        decoration: BoxDecoration(
          color: color.withAlpha(90),
          border: Border.all(color: Colors.white, width: 3),
          borderRadius: BorderRadius.circular(50),
        ),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontFamily: 'NunitoSans',
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
