import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Input extends StatelessWidget {
  final String label;
  final int maxLines;
  final bool isSecure;
  final Function(String) valid;
  final TextInputType inputType;
  final List<TextInputFormatter> inputFormatters;

  const Input({
    Key key,
    @required this.label,
    this.isSecure = false,
    @required this.valid,
    this.inputType = TextInputType.text,
    this.inputFormatters,
    this.maxLines = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: valid,
      maxLines: maxLines,
      obscureText: isSecure,
      keyboardType: inputType,
      style: TextStyle(
        fontSize: 22.0,
        fontFamily: 'Montserrat',
        fontWeight: FontWeight.bold,
      ),
      decoration: new InputDecoration(
        filled: true,
        hintText: label,
        errorStyle: TextStyle(
          fontSize: 14.0,
          color: Colors.white,
          fontFamily: 'Montserrat',
        ),
        fillColor: Colors.white,
        border: OutlineInputBorder(
          borderSide: BorderSide(width: 0.0, style: BorderStyle.none),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      inputFormatters: inputFormatters,
    );
  }
}
