import 'package:flutter/material.dart';

class Images extends StatelessWidget {
  final String urlToImage;
  final double width;
  final double height;
  final BoxFit fit;

  const Images({
    this.urlToImage,
    this.width = 100.0,
    this.height = 100.0,
    this.fit = BoxFit.cover,
  });

  @override
  Widget build(BuildContext context) {
    return (urlToImage != null)
        ? FadeInImage(
            placeholder: AssetImage('assets/images/giphy.gif'),
            image: NetworkImage(urlToImage),
            width: this.width,
            height: this.height,
            fit: this.fit,
          )
        : Image(
            image: AssetImage('assets/images/no-image.png'),
            width: this.width,
            height: this.height,
            fit: this.fit,
          );
  }
}
