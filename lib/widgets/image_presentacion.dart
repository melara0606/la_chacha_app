import 'package:flutter/material.dart';

class ImagePresentacion extends StatelessWidget {
  final double posited;

  const ImagePresentacion({this.posited});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Positioned(
          top: this.posited,
          child: Image.asset(
            "assets/images/nubes.png",
            width: size.width,
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          top: this.posited,
          child: Image.asset(
            "assets/images/logo-chacha.png",
            width: size.width,
            fit: BoxFit.cover,
          ),
        )
      ],
    );
  }
}
