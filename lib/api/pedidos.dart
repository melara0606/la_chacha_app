import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart' show required;

import './app_confi.dart';
import '../utils/session.dart';

import 'package:la_chacha_app/models/shopping_cart.dart';

class PedidosAPI {
  final _session = new Session();

  Future<bool> register({@required ShoppingCart pedido}) async {
    final url = "${AppConfig.apiHost}/pedidos";
    final _token = await _session.get();
    print(pedido.pagar);
    final response = await http.post(
      url,
      body: jsonEncode({
        "shoppingCart": pedido.toJson(),
      }),
      headers: {
        "token": _token['token'],
        'Content-Type': 'application/json',
      },
    );
    final parsed = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return parsed['ok'];
    } else if (response.statusCode == 500) {
      throw PlatformException(code: "500", message: parsed['message']);
    }
    throw PlatformException(code: "201", message: 'Error /pedidos/post');
  }
}
