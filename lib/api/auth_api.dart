import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart' show required;

import './app_confi.dart';
import '../utils/dialog.dart';
import '../utils/session.dart';

class AuthAPI {
  final _session = new Session();

  Future<bool> register(
    BuildContext context, {
    @required String nombres,
    @required String apellidos,
    @required String phone,
    @required String direccion,
  }) async {
    try {
      final url = "${AppConfig.apiHost}/users/register";
      final response = await http.post(url,
          body: jsonEncode({
            'nombres': nombres,
            'telefono': phone,
            'apellidos': apellidos,
            'direccion': direccion
          }),
          headers: {
            'Content-Type': 'application/json',
          });

      final parsed = jsonDecode(response.body);
      if (response.statusCode == 200) {
        final token = parsed['token'] as String;
        final expiresIn = parsed['expiresIn'] as int;
        await _session.set(token, expiresIn);
        return true;
      } else if (response.statusCode == 500) {
        throw PlatformException(code: "500", message: parsed['message']);
      }
      throw PlatformException(code: "201", message: 'Error /register');
    } on PlatformException catch (e) {
      Dialogs.alert(context, title: 'ERROR', message: e.message);
      return false;
    }
  }

  Future<bool> login(
    BuildContext context, {
    @required String phone,
  }) async {
    try {
      final url = "${AppConfig.apiHost}/users/login";
      final response = await http.post(url, body: {'phone': phone});

      final parsed = jsonDecode(response.body);
      if (response.statusCode == 200) {
        final token = parsed['token'] as String;
        final expiresIn = parsed['expiresIn'] as int;
        await _session.set(token, expiresIn);
        return true;
      } else if (response.statusCode == 500) {
        throw PlatformException(code: "500", message: parsed['message']);
      }
      throw PlatformException(code: "201", message: 'Error /login');
    } on PlatformException catch (e) {
      Dialogs.alert(context, title: 'ERROR', message: e.message);
      print("Error ${e.code}:${e.message}");
      return false;
    }
  }

  Future<String> getAccessToken() async {
    try {
      final result = await _session.get();
      if (result != null) {
        final token = result['token'];
        final expiresIn = result['expiresIn'] as int;
        final createdAt = DateTime.parse(result['createAt']);
        final currentDate = DateTime.now();

        final diff = currentDate.difference(createdAt).inSeconds;
        if (expiresIn - diff >= 60) {
          return token;
        }

        // final newData = await _refreshToken(token);
        // if (newData != null) {
        //   final newToken = newData['token'];
        //   final newExpiresIn = newData['expiresIn'] as int;
        //   await _session.set(newToken, newExpiresIn);
        //   return newToken;
        // }
        return null;
      }
      return null;
    } on PlatformException catch (e) {
      print("Error ${e.code}:${e.message}");
    }
  }
  /*
  _registerToken(String token) async {
    try {
      final url = "${AppConfig.apiHost}/tokens/register";
      final response = await http.post(url, headers: {'token': token});

      final parsed = jsonDecode(response.body);
      if (response.statusCode == 200) {
        return;
      } else if (response.statusCode == 500) {
        throw PlatformException(code: "500", message: parsed['message']);
      }
      throw PlatformException(code: "201", message: 'Error /tokens/register');
    } on PlatformException catch (e) {
      print("Error ${e.code}:${e.message}");
      return null;
    }
  }
  Future<dynamic> _refreshToken(String expiredToken) async {
    try {
      final url = "${AppConfig.apiHost}/tokens/refresh";
      final response = await http.post(url, headers: {'token': expiredToken});

      final parsed = jsonDecode(response.body);
      if (response.statusCode == 200) {
        return parsed;
      } else if (response.statusCode == 500) {
        throw PlatformException(code: "500", message: parsed['message']);
      }
      throw PlatformException(code: "201", message: 'Error /tokens/refresh');
    } on PlatformException catch (e) {
      print("Error ${e.code}:${e.message}");
      return null;
    }
  }
  */
}
