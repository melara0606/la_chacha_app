import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:la_chacha_app/api/auth_api.dart';
import 'package:la_chacha_app/screens/register.dart';

import 'package:la_chacha_app/utils/utils.dart';
import 'package:la_chacha_app/widgets/button.dart';
import 'package:la_chacha_app/widgets/input.dart';
import 'package:la_chacha_app/widgets/image_presentacion.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _phoneNumber;
  bool _isFetching = false;
  final AuthAPI _authAPI = new AuthAPI();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  _onSubmit() async {
    if (_isFetching) return;
    final _ok = _formKey.currentState.validate();
    if (_ok) {
      setState(() => _isFetching = true);
      final _isOk = await _authAPI.login(context, phone: _phoneNumber);
      setState(() => _isFetching = false);
      if (_isOk) {
        Navigator.pushNamedAndRemoveUntil(context, "splash", (_) => false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Container(
          width: size.width,
          height: size.height,
          color: Color(0xFFE52428),
          child: Stack(
            children: <Widget>[
              ImagePresentacion(
                posited: 120.0,
              ),
              SingleChildScrollView(
                padding: EdgeInsets.only(top: 160.0),
                child: Container(
                  // color: Colors.yellowAccent,
                  width: size.width,
                  height: size.height * 0.75,
                  child: SafeArea(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Bienvenidos',
                          style: TextStyle(
                            fontSize: 40.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                        Text(
                          'Entrar a tu cuenta',
                          style: TextStyle(
                            fontSize: 20.0,
                            height: 1.5,
                            color: Colors.white,
                            fontFamily: 'NunitoSans',
                          ),
                        ),
                        SizedBox(height: 50.0),
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            maxHeight: 350.0,
                            maxWidth: 350.0,
                          ),
                          child: Form(
                            key: _formKey,
                            child: Input(
                              inputType: TextInputType.phone,
                              label: 'Numero de telefono',
                              valid: (String value) {
                                final _length = value.length;
                                if (value.isEmpty || _length < 16) {
                                  return 'Es requerido un numero de telefono valido';
                                }
                                _phoneNumber = value;
                                return null;
                              },
                              inputFormatters: [Utils.inputMarks()],
                            ),
                          ),
                        ),
                        ConstrainedBox(
                          constraints: BoxConstraints(maxWidth: 350.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              SizedBox(height: 30.0),
                              Button(
                                title: 'INGRESAR',
                                onTap: _onSubmit,
                                color: Color(0xFFE52428),
                              ),
                              SizedBox(height: 60.0),
                              Text(
                                '¿Aun no tienes cuenta?',
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.white,
                                  fontFamily: 'NunitoSans',
                                ),
                              ),
                              SizedBox(width: 5.0),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => RegisterPage()),
                                  );
                                },
                                child: Text(
                                  ' Registrate',
                                  style: TextStyle(
                                    fontSize: 22.0,
                                    color: Colors.green,
                                    fontFamily: 'NunitoSans',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              _isFetching
                  ? Positioned.fill(
                      child: Container(
                        color: Colors.black45,
                        child: Center(
                          child: CupertinoActivityIndicator(radius: 15),
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}
