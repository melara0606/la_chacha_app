import 'package:flutter/material.dart';
import 'package:la_chacha_app/screens/login_page.dart';
import 'package:la_chacha_app/screens/register.dart';
import 'package:la_chacha_app/widgets/button.dart';
import 'package:la_chacha_app/app_properties.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: ColorPrimary.withOpacity(0.6),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              width: size.width,
              height: size.height * 0.50,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  end: FractionalOffset.bottomCenter,
                  begin: FractionalOffset.topCenter,
                  colors: [
                    const Color(0xff000000).withOpacity(0),
                    const Color(0xff000000),
                  ],
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                  bottom: 50.0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Button(
                      title: 'Login in',
                      color: buttonPrimary,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => LoginPage(),
                          ),
                        );
                      },
                    ),
                    SizedBox(height: 10.0),
                    Button(
                      title: 'Register',
                      color: buttonSecond,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (_) => RegisterPage()),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
