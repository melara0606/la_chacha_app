import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:la_chacha_app/api/pedidos.dart';
import 'package:la_chacha_app/utils/dialog.dart';
import 'package:la_chacha_app/services/shopping_cart_service.dart';
import 'package:la_chacha_app/screens/shopping_cart/components/shopping_list_cart.dart';

class Orderpage extends StatefulWidget {
  Orderpage({Key key}) : super(key: key);

  @override
  _OrderpageState createState() => _OrderpageState();
}

class _OrderpageState extends State<Orderpage> {
  PedidosAPI _pedidosAPI = PedidosAPI();

  _pedidos() async {
    try {
      final _shoppingCart =
          Provider.of<ShoppingCartService>(context, listen: false);
      await _pedidosAPI.register(pedido: _shoppingCart.shoppingCart);
      _shoppingCart.isClear = true;
      Dialogs.alertShoppingCart(context);
    } on Exception catch (e) {
      Dialogs.alert(context, title: "Error", message: e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    final _shoppingCart =
        Provider.of<ShoppingCartService>(context).shoppingCart;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        title: Text(
          _shoppingCart.negocio == null
              ? "No hay orden aun"
              : "Tu order en ${_shoppingCart.negocio.nombre}",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20.0,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        leading: CloseButton(),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30.0),
                  ShoppingList(list: _shoppingCart.list),
                  SizedBox(height: 16.0),
                  _shoppingCart.list.length == 0
                      ? Container()
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'SubTotal',
                              style: TextStyle(
                                fontSize: 22.0,
                                fontFamily: 'NunitoSans',
                                color: Colors.black,
                              ),
                            ),
                            Text(
                              '\$ ${_shoppingCart.total().toStringAsFixed(2)}',
                              style: TextStyle(
                                fontSize: 22.0,
                                fontFamily: 'NunitoSans',
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            )
                          ],
                        ),
                  SizedBox(height: 20.0),
                ],
              ),
            ),
          ),
          SafeArea(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: _shoppingCart.list.length > 0
                  ? Container(
                      height: 80.0,
                      width: double.infinity,
                      padding: EdgeInsets.all(16),
                      child: FlatButton(
                        onPressed: _pedidos,
                        child: Text(
                          'PROCESAR ORDEN',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Montserrat",
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        color: Colors.pinkAccent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4),
                        ),
                      ),
                    )
                  : Container(),
            ),
          )
        ],
      ),
    );
  }
}
