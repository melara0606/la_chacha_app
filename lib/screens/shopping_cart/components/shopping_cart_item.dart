import 'package:flutter/material.dart';
import 'package:la_chacha_app/models/shopping_cart.dart';

class ShoppingCartItem extends StatelessWidget {
  final ItemShoppingCart item;
  const ShoppingCartItem({this.item});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 24,
              width: 24,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey.shade300,
                ),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Center(
                child: Text(
                  '${item.cantida}',
                  // textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                  ),
                ),
              ),
            ),
            SizedBox(width: 8.0),
            Text('*'),
            SizedBox(width: 8.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    item.product.nombreProducto,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 8.0),
                  Text(
                    item.toString(),
                    style: TextStyle(
                      color: Colors.grey.shade500,
                      fontSize: 12.0,
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Text(
                "\$${(item.total * item.cantida).toStringAsFixed(2)}",
                style: TextStyle(
                  color: Colors.amber,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 14.0),
        Divider(
          thickness: 1.3,
          color: Colors.grey.shade200,
          height: 1,
        ),
      ],
    );
  }
}
