import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:la_chacha_app/utils/dialog.dart';
import 'package:la_chacha_app/models/shopping_cart.dart';
import 'package:la_chacha_app/services/shopping_cart_service.dart';
import 'package:la_chacha_app/screens/shopping_cart/components/shopping_cart_item.dart';

class ShoppingList extends StatelessWidget {
  final List<ItemShoppingCart> list;
  const ShoppingList({this.list});

  @override
  Widget build(BuildContext context) {
    if (list.isEmpty) {
      return Container(
        padding: EdgeInsets.all(10.0),
        color: Colors.pink[600],
        child: Center(
          child: Text(
            'No tienes producto seleccionado por el momento',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 18.0),
          ),
        ),
      );
    }
    return ListView.builder(
      itemCount: list.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        final item = list[index];
        return Dismissible(
          confirmDismiss: (direction) async {
            if (direction == DismissDirection.endToStart) {
              final bool res = await Dialogs.confirm(
                context,
                title: "Eliminar",
                message:
                    "Esta seguro que desea eliminar a \"${item.product.nombreProducto}\"",
                onCancel: () {
                  Navigator.of(context).pop(false);
                },
                onConfirm: () {
                  final _shoppingCart =
                      Provider.of<ShoppingCartService>(context, listen: false);
                  _shoppingCart.shoppingCart.listItemDelete(item);
                  _shoppingCart.pagar = true;
                  Navigator.of(context).pop(true);
                },
              );
              return res;
            }
            return false;
          },
          background: slideLeftBackground(),
          child: ShoppingCartItem(item: item),
          key: Key(item.product.nombreProducto),
          direction: DismissDirection.endToStart,
        );
      },
    );
  }

  Widget slideLeftBackground() {
    return Container(
      color: Colors.pinkAccent,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.delete,
              color: Colors.white,
            ),
            Text(
              " Eliminar",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
        alignment: Alignment.centerRight,
      ),
    );
  }
}
