import 'package:flutter/material.dart';
import 'package:la_chacha_app/screens/intro_page.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class VerifyCodePage extends StatefulWidget {
  VerifyCodePage({Key key}) : super(key: key);

  @override
  _VerifyCodePageState createState() => _VerifyCodePageState();
}

class _VerifyCodePageState extends State<VerifyCodePage> {
  _onComplete(String code) {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (_) => IntroPage()),
      (_) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              "Verifica tu numero",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 36.0,
                fontFamily: 'Montserrat',
              ),
            ),
          ),
          // SizedBox(height: 10.0),
          Padding(
            padding: EdgeInsets.only(left: 60.0, right: 60.0),
            child: Center(
              child: Text(
                'Introduce el codigo enviado',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                  fontFamily: 'Overpass',
                ),
              ),
            ),
          ),
          SizedBox(height: 20.0),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
            child: PinCodeTextField(
              length: 6,
              autoFocus: true,
              fieldWidth: 50,
              fieldHeight: 80,
              enableActiveFill: true,
              inactiveColor: Colors.grey[300],
              inactiveFillColor: Colors.grey[300],
              selectedFillColor: Colors.grey[400],
              activeFillColor: Colors.green[500],
              animationDuration: Duration(milliseconds: 300),
              textStyle: TextStyle(fontSize: 30, color: Colors.white),
              onCompleted: _onComplete,
              onChanged: (value) {},
            ),
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "¿No recibió el código?",
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 18.0,
                  fontFamily: 'NunitoSans',
                ),
              ),
              SizedBox(width: 10.0),
              Text(
                'Reenviar el codigo',
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 18.0,
                  fontFamily: 'NunitoSans',
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
