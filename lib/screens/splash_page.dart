import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:la_chacha_app/api/auth_api.dart';

// import 'package:provider/provider.dart';
import 'package:la_chacha_app/app_properties.dart';
import 'package:la_chacha_app/screens/home/home.dart';
// import 'package:la_chacha_app/services/categorys_services.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  final _authApi = AuthAPI();

  @override
  void initState() {
    super.initState();
    check();
  }

  check() async {
    final token = await _authApi.getAccessToken();
    if (token != null) {
      Navigator.pushReplacementNamed(context, "main");
    } else {
      Navigator.pushReplacementNamed(context, "login");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: ColorPrimary),
      child: Container(
        decoration: BoxDecoration(color: ColorPrimary),
        child: SafeArea(
          child: Scaffold(
            backgroundColor: ColorPrimary,
            body: Column(
              children: <Widget>[
                Expanded(
                  child: Opacity(
                    opacity: 0.8,
                    child: Image.asset("assets/images/logo-chacha.png"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Center(
                    child: CupertinoActivityIndicator(
                      radius: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
