import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:la_chacha_app/widgets/image.dart';
import 'package:la_chacha_app/models/product.dart';
import 'package:la_chacha_app/services/products_service.dart';
import 'package:la_chacha_app/services/categorys_services.dart';

import '../product.dart';

class ProductItem extends StatelessWidget {
  final Product product;

  const ProductItem({this.product});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final _product = Provider.of<ProductsService>(context, listen: false);
        final _category = Provider.of<CategorysService>(context, listen: false);
        _product.product = this.product;

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => ProductPage(
              product: this.product,
              negocio: _category.negocioSelected,
            ),
          ),
        );
      },
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 40.0),
            padding: EdgeInsets.only(left: 100),
            height: 140.0,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: Text(
                        product.nombreProducto,
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          "\$${product.precio.toStringAsFixed(2)}",
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 5.0),
                Text(
                  product.descripcion,
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 20.0,
            top: 15.0,
            bottom: 15.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Images(urlToImage: product.urlToImage),
            ),
          )
        ],
      ),
    );
  }
}
