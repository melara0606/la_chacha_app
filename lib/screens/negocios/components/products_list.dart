import 'package:flutter/material.dart';
import 'package:la_chacha_app/models/product.dart';
import 'package:la_chacha_app/screens/negocios/components/product_item.dart';

class ProductsList extends StatelessWidget {
  final List<Product> lista;
  const ProductsList({this.lista});

  @override
  Widget build(BuildContext context) {
    if (lista.length == 0) {
      return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Por el momento este negocio no tiene productos',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20.0,
                textBaseline: TextBaseline.alphabetic,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      );
    }

    return ListView.builder(
      itemCount: lista.length,
      padding: EdgeInsets.fromLTRB(0, 120, 30, 20),
      itemBuilder: (BuildContext context, int index) {
        return ProductItem(product: lista[index]);
      },
    );
  }
}
