import 'package:flutter/material.dart';
import 'package:la_chacha_app/models/product_characteristic.dart';
import 'package:la_chacha_app/screens/negocios/components/item_group_radio.dart';

class GroupByRadio extends StatelessWidget {
  final List<ProductCharacteristic> lista;

  final Function onChange;
  final Function onChangeMultiple;
  const GroupByRadio({this.lista, this.onChange, this.onChangeMultiple});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: lista.length,
        itemBuilder: (BuildContext context, int index) {
          ProductCharacteristic productCharacteristic = lista[index];
          return Column(
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  productCharacteristic.label,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Divider(),
              ItemGroupRadio(
                onChange: this.onChange,
                element: productCharacteristic,
                items: productCharacteristic.items.elementAt(0),
                multiple: productCharacteristic.multiple,
                onChangeMultiple: this.onChangeMultiple,
              ),
              SizedBox(height: 30.0)
            ],
          );
        },
      ),
    );
  }
}
