import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:la_chacha_app/models/product_characteristic.dart';

class ItemGroupRadio extends StatelessWidget {
  final bool multiple;
  final List<Item> items;
  final Function onChange;
  final ProductCharacteristic element;
  final Function onChangeMultiple;
  const ItemGroupRadio({
    this.items,
    this.element,
    this.multiple,
    this.onChange,
    this.onChangeMultiple,
  });

  @override
  Widget build(BuildContext context) {
    if (this.multiple) {
      return Container(
        child: CheckboxGroup(
          activeColor: Colors.pinkAccent,
          labelStyle: TextStyle(
            fontSize: 17.0,
            fontFamily: 'Montserrat',
          ),
          labels: items.map((Item element) {
            return element.label;
          }).toList(),
          itemBuilder: (Checkbox cb, Text txt, int i) {
            Item _item = items.elementAt(i);
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                cb,
                txt,
                Expanded(
                  child: Container(
                    child: _item.isPrice
                        ? Text(
                            "+\$ ${_item.precio.toStringAsFixed(2)}",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Montserrat',
                            ),
                          )
                        : Text(''),
                  ),
                ),
              ],
            );
          },
          onChange: (bool isChecked, String label, int index) {
            Item elementIndex = items.elementAt(index);
            this.onChangeMultiple(elementIndex, this.element, isChecked);
          },
        ),
      );
    }

    return Container(
      child: RadioButtonGroup(
        activeColor: Colors.pinkAccent,
        labelStyle: TextStyle(
          fontSize: 17.0,
          fontFamily: 'Montserrat',
        ),
        labels: items.map((Item element) {
          return element.label;
        }).toList(),
        itemBuilder: (Radio cb, Text txt, int i) {
          Item _item = items.elementAt(i);
          return Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              cb,
              txt,
              Expanded(
                child: Container(
                  child: _item.isPrice
                      ? Text(
                          "+\$ ${_item.precio.toStringAsFixed(2)}",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        )
                      : Text(''),
                ),
              ),
            ],
          );
        },
        onChange: (String label, int index) {
          Item elementIndex = items.elementAt(index);
          this.onChange(elementIndex, this.element);
        },
      ),
    );
  }
}
