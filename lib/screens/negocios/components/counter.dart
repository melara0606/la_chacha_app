import 'package:flutter/material.dart';

class Counter extends StatelessWidget {
  final int counter;
  final Function increment;
  final Function descrement;

  const Counter({this.counter, this.increment, this.descrement});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          child: Container(
            padding: const EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.black,
            ),
            child: Icon(
              Icons.remove,
              color: Colors.white,
              size: 30.0,
            ),
          ),
          onTap: this.descrement,
        ),
        // SizedBox(width: 15),
        Container(
          height: 54.0,
          width: 70.0,
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          child: Center(
            child: Text(
              "$counter",
              style: TextStyle(
                fontSize: 28.0,
                fontWeight: FontWeight.bold,
                fontFamily: "Montserrat",
              ),
            ),
          ),
        ),
        // SizedBox(width: 15),
        GestureDetector(
          child: Container(
            padding: const EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.black,
            ),
            child: Icon(
              Icons.add,
              color: Colors.white,
              size: 30.0,
            ),
          ),
          onTap: this.increment,
        ),
      ],
    );
  }
}
