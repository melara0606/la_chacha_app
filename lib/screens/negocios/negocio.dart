import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:la_chacha_app/widgets/image.dart';
import 'package:la_chacha_app/models/negocio.dart';
import 'package:la_chacha_app/widgets/loading.dart';
import 'package:la_chacha_app/widgets/scale_route.dart';
import 'package:la_chacha_app/services/categorys_services.dart';
import 'package:la_chacha_app/screens/shopping_cart/order_shopping.dart';
import 'package:la_chacha_app/screens/negocios/components/products_list.dart';

class NegocioPage extends StatefulWidget {
  final Negocio negocio;

  NegocioPage({this.negocio});

  @override
  _NegocioPageState createState() => _NegocioPageState();
}

class _NegocioPageState extends State<NegocioPage> {
  @override
  Widget build(BuildContext context) {
    final _categoria = Provider.of<CategorysService>(context);

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: true,
              pinned: true,
              backgroundColor: Colors.pink,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                background: Images(urlToImage: widget.negocio.urlToImage),
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.shopping_cart, color: Colors.black),
                  onPressed: () {
                    Navigator.push(
                      context,
                      ScaleRoute(page: Orderpage()),
                    );
                  },
                )
              ],
            )
          ];
        },
        body: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 20.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.negocio.nombre,
                        style: TextStyle(
                          fontSize: 23.0,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: 140.0,
                        child: Text(
                          "Minimo: \$${double.parse(widget.negocio.minimo.toString()).toStringAsFixed(2)}",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.orange.withOpacity(0.2),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: 7.0,
                            right: 7.0,
                            top: 3.0,
                            bottom: 3.0,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.access_time,
                                color: Colors.orange,
                              ),
                              SizedBox(width: 5.0),
                              Text(
                                widget.negocio.hora(),
                                style: TextStyle(
                                  color: Colors.orange,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            _categoria.negocioLoading
                ? Loading()
                : ProductsList(lista: _categoria.listProducts),
          ],
        ),
      ),
    );
  }
}
