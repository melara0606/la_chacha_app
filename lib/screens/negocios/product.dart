import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:la_chacha_app/utils/dialog.dart';
import 'package:la_chacha_app/widgets/image.dart';
import "package:la_chacha_app/models/product.dart";
import 'package:la_chacha_app/models/negocio.dart';
import 'package:la_chacha_app/widgets/loading.dart';
import 'package:la_chacha_app/models/shopping_cart.dart';
import 'package:la_chacha_app/services/products_service.dart';
import "package:la_chacha_app/models/product_characteristic.dart";
import 'package:la_chacha_app/services/shopping_cart_service.dart';
import 'package:la_chacha_app/screens/negocios/components/counter.dart';
import 'package:la_chacha_app/screens/negocios/components/group_by_radio.dart';

class ProductPage extends StatefulWidget {
  final Product product;
  final Negocio negocio;

  ItemShoppingCart shoppingCart;

  ProductPage({this.product, this.negocio}) {
    this.shoppingCart = ItemShoppingCart(product: this.product);
  }

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  double total;
  int cantidad;

  @override
  void initState() {
    super.initState();
    this.cantidad = 1;
    this.total = widget.shoppingCart.total;
  }

  onAddProduct() {
    final shoppingCart =
        Provider.of<ShoppingCartService>(context, listen: false);

    if (this._validate(widget.shoppingCart.characteristics.length)) {
      try {
        widget.shoppingCart.cantida = cantidad;
        shoppingCart.shoppingCart.negocio = widget.negocio;
        shoppingCart.shoppingCart.list.add(widget.shoppingCart);
        Navigator.pop(context);
      } catch (e) {
        Dialogs.confirm(
          context,
          title: 'Orden Iniciada',
          message:
              'Ya tienes una orden en processo, ¿Deseas eliminar y crear una orden nueva con este comercio?',
          onCancel: () {
            Navigator.pop(context);
          },
          onConfirm: () {
            shoppingCart.shoppingCart
                .setNegocioForze(widget.negocio, widget.shoppingCart);
            Navigator.pop(context);
            Navigator.pop(context);
          },
        );
      }
    } else {
      Dialogs.alert(
        context,
        title: "No valido",
        message: "Es necesario que eligas todas las opciones",
      );
    }
  }

  onChangeMultiple(Item item, ProductCharacteristic element, bool isChecked) {
    widget.shoppingCart.addMultiple(element, item, isChecked);
    this._setValue();
  }

  onChange(Item item, ProductCharacteristic element) {
    widget.shoppingCart.add(element, item);
    this._setValue();
  }

  _setValue() {
    setState(() {
      total = widget.shoppingCart.total * cantidad;
    });
  }

  _validate(int cantidad) {
    final size =
        Provider.of<ProductsService>(context, listen: false).list.length;
    return cantidad == size;
  }

  @override
  Widget build(BuildContext context) {
    final _productService = Provider.of<ProductsService>(context);
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: true,
              pinned: true,
              backgroundColor: Colors.pink,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                background: Images(
                  urlToImage: widget.product.urlToImage,
                ),
              ),
            )
          ];
        },
        body: Stack(
          children: <Widget>[
            _productService.isLoading
                ? Loading()
                : Container(
                    padding: EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 50.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              widget.product.nombreProducto,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                            Text(
                              "\$ ${widget.product.precio.toStringAsFixed(2)} ",
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat',
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10.0),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                widget.product.descripcion,
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontFamily: 'Montserrat',
                                ),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20.0),
                        Expanded(
                          child: GroupByRadio(
                            onChange: onChange,
                            lista: _productService.list,
                            onChangeMultiple: onChangeMultiple,
                          ),
                        ),
                        SizedBox(
                          height: 85.0,
                          child: Counter(
                            counter: cantidad,
                            increment: () {
                              setState(() {
                                cantidad++;
                                total = widget.shoppingCart.total * cantidad;
                              });
                            },
                            descrement: () {
                              setState(() {
                                cantidad = cantidad == 1 ? 1 : cantidad - 1;
                                total = widget.shoppingCart.total * cantidad;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
            _productService.isLoading
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: CupertinoButton(
                      minSize: 60,
                      borderRadius: BorderRadius.all(Radius.zero),
                      color: Colors.pinkAccent,
                      onPressed: onAddProduct,
                      child: Text(
                        'Agregar  $cantidad a la orden - \$${total.toStringAsFixed(2)}',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                  )
          ],
        ),
      ),
    );
  }
}
