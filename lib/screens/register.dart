import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:la_chacha_app/screens/verify_code.dart';

import 'package:la_chacha_app/utils/utils.dart';
import 'package:la_chacha_app/widgets/input.dart';
import 'package:la_chacha_app/widgets/button.dart';
import 'package:la_chacha_app/api/auth_api.dart';
import 'package:la_chacha_app/widgets/image_presentacion.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  bool _isFetching = false;
  final _authApi = new AuthAPI();
  String _names, _lasts, _phoneNumber, _direccion;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  void _onRegister() async {
    if (_isFetching) return;
    final ok = _formKey.currentState.validate();
    print(ok);
    if (ok) {
      setState(() => _isFetching = true);
      final _isOk = await _authApi.register(
        context,
        nombres: _names,
        apellidos: _lasts,
        phone: _phoneNumber,
        direccion: _direccion,
      );
      if (_isOk) {
        Timer(Duration(milliseconds: 3000), () {
          setState(() => _isFetching = false);
          Navigator.pushNamedAndRemoveUntil(context, "splash", (_) => false);
        });
      } else {
        setState(() => _isFetching = false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFE52428),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: 30.0,
          ),
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Container(
          color: Color(0xFFE52428),
          width: size.width,
          height: size.height,
          child: Stack(
            children: <Widget>[
              ImagePresentacion(posited: 2.0),
              SingleChildScrollView(
                // padding: EdgeInsets.only(top: 180.0),
                child: Container(
                  width: size.width,
                  height: size.height,
                  // color: Colors.yellow,
                  child: SafeArea(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Registrate",
                          style: TextStyle(
                            fontSize: 40.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                        SizedBox(height: 20.0),
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            maxWidth: 350.0,
                          ),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                Input(
                                  label: 'Nombres',
                                  valid: (String value) {
                                    if (!(value.isNotEmpty &&
                                        value.length > 3)) {
                                      return 'Debes ingresar nombres validos';
                                    }
                                    _names = value;
                                    return null;
                                  },
                                ),
                                SizedBox(height: 10),
                                Input(
                                  label: 'Apellidos',
                                  valid: (String value) {
                                    if (!(value.isNotEmpty &&
                                        value.length > 3)) {
                                      return 'Debes ingresar apellidos validos';
                                    }
                                    _lasts = value;
                                    return null;
                                  },
                                ),
                                SizedBox(height: 10),
                                Input(
                                  label: 'Numero de telefono',
                                  inputType: TextInputType.phone,
                                  inputFormatters: [Utils.inputMarks()],
                                  valid: (String value) {
                                    final _length = value.length;
                                    if (value.isEmpty || _length < 16) {
                                      return 'Es requerido un numero de telefono valido';
                                    }
                                    _phoneNumber = value;
                                    return null;
                                  },
                                ),
                                SizedBox(height: 10),
                                Input(
                                  maxLines: 3,
                                  label: 'Direccion',
                                  inputType: TextInputType.multiline,
                                  valid: (String value) {
                                    if (value.isEmpty) {
                                      return 'Es requerida una direccion';
                                    }
                                    _direccion = value;
                                    return null;
                                  },
                                ),
                                SizedBox(height: 20.0),
                                Button(
                                  title: 'Registrate',
                                  onTap: _onRegister,
                                  color: Color(0xFFE52428),
                                ),
                                SizedBox(height: 20.0),
                                Text(
                                  'Al hacer clic en registrar, acepta los términos y condiciones',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 17.0,
                                    height: 1.5,
                                    color: Colors.white,
                                    fontFamily: 'NunitoSans',
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              _isFetching
                  ? Positioned.fill(
                      child: Container(
                        color: Colors.black45,
                        child: Center(
                          child: CupertinoActivityIndicator(radius: 15),
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}
