import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:la_chacha_app/models/negocio.dart';
import 'package:la_chacha_app/widgets/scale_route.dart';
import 'package:la_chacha_app/screens/negocios/negocio.dart';
import 'package:la_chacha_app/services/categorys_services.dart';

class HeaderListPage extends StatelessWidget {
  final List<Negocio> lista;
  const HeaderListPage({this.lista});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        final item = this.lista[index];
        EdgeInsets padding = index == 0
            ? const EdgeInsets.only(
                left: 20.0, right: 10.0, top: 4.0, bottom: 30.0)
            : const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 4.0, bottom: 30.0);

        return Padding(
          padding: padding,
          child: InkWell(
            onTap: () {
              final _categoria =
                  Provider.of<CategorysService>(context, listen: false);
              _categoria.negocioSelected = item;
              Navigator.push(
                context,
                ScaleRoute(page: NegocioPage(negocio: item)),
              );
            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withAlpha(70),
                    offset: const Offset(3.0, 10.0),
                    blurRadius: 15.0,
                  ),
                ],
                image: DecorationImage(
                  image: item.urlToImage == null
                      ? ExactAssetImage("assets/images/no-image.png")
                      : NetworkImage(item.urlToImage),
                  fit: BoxFit.cover,
                ),
              ),
              width: 200.0,
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      decoration: BoxDecoration(
                        color: const Color(0xFF273A48),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0),
                        ),
                      ),
                      height: 30.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            '${item.nombre}',
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
      scrollDirection: Axis.horizontal,
      itemCount: this.lista.length,
    );
  }
}
