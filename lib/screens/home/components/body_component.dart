import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:la_chacha_app/models/negocio.dart';
import 'package:la_chacha_app/widgets/scale_route.dart';
import 'package:la_chacha_app/screens/negocios/negocio.dart';
import 'package:la_chacha_app/services/categorys_services.dart';
import 'package:la_chacha_app/screens/home/components/header_list.dart';
import 'package:la_chacha_app/screens/shopping_cart/order_shopping.dart';

class BodyComponent extends StatelessWidget {
  final List<Negocio> nuevosNegocios;
  final List<Negocio> negocios;

  const BodyComponent({this.nuevosNegocios, this.negocios});

  @override
  Widget build(BuildContext context) {
    final _headerList = HeaderListPage(lista: nuevosNegocios);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "La chacha",
          style: TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            fontFamily: "Montserrat",
          ),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart, color: Colors.white),
            onPressed: () {
              Navigator.push(
                context,
                ScaleRoute(page: Orderpage()),
              );
            },
          )
        ],
      ),
      backgroundColor: Colors.transparent,
      body: Container(
        child: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 18.0),
                      child: Text(
                        'Nuevos Socios',
                        style: TextStyle(
                          color: Colors.white70,
                          fontFamily: "NunitoSans",
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Container(
                    height: 250.0,
                    width: MediaQuery.of(context).size.width,
                    child: _headerList,
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        final _item = negocios[index];
                        return GestureDetector(
                          onTap: () {
                            final _category = Provider.of<CategorysService>(
                                context,
                                listen: false);
                            _category.negocioSelected = _item;
                            Navigator.push(
                              context,
                              ScaleRoute(page: NegocioPage(negocio: _item)),
                            );
                          },
                          child: ListTile(
                            title: Column(
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      height: 100.0,
                                      width: 100.0,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.black.withAlpha(70),
                                            offset: const Offset(2.0, 2.0),
                                            blurRadius: 2.0,
                                          )
                                        ],
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(12.0),
                                        ),
                                        image: DecorationImage(
                                          image: _item.urlToImage == null
                                              ? ExactAssetImage(
                                                  "assets/images/no-image.png")
                                              : NetworkImage(_item.urlToImage),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 20.0),
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            _item.nombre,
                                            style: TextStyle(
                                              fontSize: 18.0,
                                              color: Colors.black87,
                                              fontFamily: "Montserrat",
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            _item.descripcion,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black54,
                                              fontFamily: "NunitoSans",
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                          SizedBox(height: 8),
                                          Text(
                                            "Minimo: \$" +
                                                _item.minimo.toString(),
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black87,
                                              fontFamily: "NunitoSans",
                                            ),
                                          ),
                                          Text(
                                            "Horario: " + _item.hora(),
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black87,
                                              fontFamily: "NunitoSans",
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20.0),
                                Divider(),
                              ],
                            ),
                          ),
                        );
                      },
                      itemCount: negocios.length,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
