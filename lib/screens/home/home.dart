import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:la_chacha_app/widgets/loading.dart';
import 'package:la_chacha_app/services/categorys_services.dart';
import 'package:la_chacha_app/screens/home/components/body_component.dart';

import 'components/background.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final _categoria = Provider.of<CategorysService>(context);

    return Container(
      decoration: BoxDecoration(
        color: Color(0xFF273A48),
      ),
      child: Stack(
        children: <Widget>[
          new CustomPaint(
            size: new Size(size.width, size.height),
            painter: new Background(),
          ),
          _categoria.isLoading
              ? Loading()
              : BodyComponent(
                  negocios: _categoria.listNegocios,
                  nuevosNegocios: _categoria.listNegociosUltimate,
                )
        ],
      ),
    );
  }
}
