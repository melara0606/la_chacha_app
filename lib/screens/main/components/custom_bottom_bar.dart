import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomBottomBar extends StatelessWidget {
  final TabController controller;
  const CustomBottomBar({Key key, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _icon(icon: 'assets/icons/home_icon.svg', index: 0, type: 'svg'),
          _icon(icon: 'assets/icons/profile_icon.png', index: 1, type: 'image'),
        ],
      ),
    );
  }

  Widget _icon({
    @required String icon,
    @required int index,
    String type = 'image',
  }) {
    return IconButton(
      icon: type == 'image'
          ? Image.asset(icon, fit: BoxFit.cover, height: 30.0)
          : SvgPicture.asset(icon, fit: BoxFit.contain, height: 40.0),
      onPressed: () {
        controller.animateTo(index);
      },
    );
  }
}
