import 'package:flutter/material.dart';
import 'package:la_chacha_app/screens/home/home.dart';
import 'package:la_chacha_app/screens/profile/profile_page.dart';
import 'package:la_chacha_app/screens/main/components/custom_bottom_bar.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with TickerProviderStateMixin<MainPage> {
  TabController bottomController;

  @override
  void initState() {
    super.initState();
    bottomController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CustomBottomBar(controller: bottomController),
      body: CustomPaint(
        child: TabBarView(
          controller: bottomController,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            HomePage(),
            ProfilePage(),
          ],
        ),
      ),
    );
  }
}
