import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:la_chacha_app/utils/session.dart';
import 'package:la_chacha_app/utils/dialog.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF9F9F9),
      body: SafeArea(
        top: true,
        child: SingleChildScrollView(
          child: Padding(
            padding:
                EdgeInsets.only(left: 16.0, right: 16.0, top: kToolbarHeight),
            child: Column(
              children: <Widget>[
                CircleAvatar(
                  maxRadius: 48,
                  backgroundImage: AssetImage('assets/images/no-image.png'),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Usuario',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(height: 20.0),
                ListTile(
                  title: Text('Salir'),
                  subtitle: Text('Privacy and logout'),
                  leading: Image.asset(
                    'assets/icons/sign_out.png',
                    fit: BoxFit.scaleDown,
                    width: 30,
                    height: 30,
                  ),
                  trailing: Icon(
                    Icons.chevron_right,
                    color: Colors.yellowAccent,
                  ),
                  onTap: () {
                    Dialogs.confirm(
                      context,
                      title: 'Confirmar',
                      message: 'Esta seguro que quieres salir?',
                      onCancel: () {
                        Navigator.pop(context);
                      },
                      onConfirm: () async {
                        Session _session = Session();
                        await _session.clearAll();
                        Navigator.pushNamedAndRemoveUntil(
                            context, "login", (_) => false);
                      },
                    );
                  },
                ),
                Divider(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/** 
class ProfilePage extends StatelessWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Center(
          child: CupertinoButton(
            color: Colors.pinkAccent,
            onPressed: () {
              
            },
            child: Text('Salir'),
          ),
        ),
      ),
    );
  }
}
*/
