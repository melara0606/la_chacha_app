import 'package:flutter/material.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  int pageIndex = 0;
  PageController controller = PageController();

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.grey[100],
          image: DecorationImage(
            image: AssetImage('assets/images/background.png'),
          ),
        ),
        child: Stack(
          children: <Widget>[
            PageView(
              onPageChanged: (value) {
                setState(() {
                  pageIndex = value;
                });
              },
              controller: controller,
              children: <Widget>[
                _pageView(
                  title: 'Obtenga  sus pedidos en linea',
                  image: 'assets/images/firstScreen.png',
                  texto:
                      'Puedes comprar cualquier cosa, desde productos digitales hasta hardware en poco clicks.',
                ),
                _pageView(
                  title: 'Envíos a cualquier lugar',
                  image: 'assets/images/secondScreen.png',
                  texto:
                      'Enviaremos a cualquier parte del mundo, con 30 días de política de devolución de dinero del 100%.',
                ),
                _pageView(
                  title: 'En un momento todo lo que necesesites',
                  image: 'assets/images/thirdScreen.png',
                  texto:
                      'Tu puedes comprar tu producto con el poder de nuestro servicio.',
                ),
              ],
            ),
            Positioned(
              bottom: 16.0,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        indicatorPage(0),
                        indicatorPage(1),
                        indicatorPage(2),
                      ],
                    ),
                    onButtons()
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Row onButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Opacity(
          opacity: pageIndex != 2 ? 1.0 : 0.0,
          child: FlatButton(
            splashColor: Colors.transparent,
            child: Text(
              'Omitir',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16,
                fontFamily: 'Montserrat',
              ),
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(context, 'main');
            },
          ),
        ),
        pageIndex != 2
            ? FlatButton(
                splashColor: Colors.transparent,
                child: Text(
                  'Siguiente',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat',
                    fontSize: 16,
                  ),
                ),
                onPressed: () {
                  if (!(controller.page == 2.0)) {
                    controller.nextPage(
                      duration: Duration(milliseconds: 200),
                      curve: Curves.easeInBack,
                    );
                  }
                },
              )
            : FlatButton(
                splashColor: Colors.transparent,
                child: Text(
                  'Finalizar',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  Navigator.pushReplacementNamed(context, 'main');
                },
              ),
      ],
    );
  }

  Container indicatorPage(int index) {
    return Container(
      margin: EdgeInsets.all(8.0),
      height: 12.0,
      width: 12.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: Colors.black,
          width: 2.0,
        ),
        color: pageIndex == index ? Colors.yellow : Colors.white,
      ),
    );
  }

  Column _pageView({@required String image, @required title, @required texto}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Center(
          child: Image.asset(
            image,
            width: 200,
            height: 200,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Text(
            title,
            textAlign: TextAlign.right,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Montserrat',
              fontSize: 16.0,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 32.0,
            vertical: 16.0,
          ),
          child: Text(
            texto,
            textAlign: TextAlign.right,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 12.0,
              fontFamily: 'Montserrat',
            ),
          ),
        )
      ],
    );
  }
}
