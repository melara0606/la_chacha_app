import 'dart:convert';

List<Product> productFromJson(String str) =>
    List<Product>.from(json.decode(str).map((x) => Product.fromJson(x)));

String productToJson(List<Product> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Product {
  int id;
  String nombreProducto;
  double precio;
  String descripcion;
  String nombreTipo;
  String urlToImage;

  Product({
    this.id,
    this.nombreProducto,
    this.precio,
    this.descripcion,
    this.nombreTipo,
    this.urlToImage,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json["id"],
      nombreProducto: json["nombre_producto"],
      precio: json["precio"].toDouble(),
      descripcion: json["descripcion"],
      nombreTipo: json["nombre_tipo"],
      urlToImage: json["urlToImage"] == null ? null : json["urlToImage"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "nombre_producto": nombreProducto,
      "precio": precio,
      "descripcion": descripcion,
      "nombre_tipo": nombreTipo,
      "urlToImage": urlToImage == null ? null : urlToImage,
    };
  }
}
