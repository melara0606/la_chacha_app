import 'dart:convert';

List<ProductCharacteristic> productCharacteristicFromJson(String str) =>
    List<ProductCharacteristic>.from(
        json.decode(str).map((x) => ProductCharacteristic.fromJson(x)));

String productCharacteristicToJson(List<ProductCharacteristic> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductCharacteristic {
  int id;
  String label;
  bool multiple;
  List<List<Item>> items;
  List<Item> item = List();

  ProductCharacteristic({
    this.id,
    this.label,
    this.multiple,
    this.items,
  });

  factory ProductCharacteristic.fromJson(Map<String, dynamic> json) {
    return ProductCharacteristic(
      id: json["id"],
      label: json["label"],
      multiple: json["multiple"],
      items: List<List<Item>>.from(
        json["items"].map(
          (x) => List<Item>.from(
            x.map((x) => Item.fromJson(x)),
          ),
        ),
      ),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "label": label,
      "multiple": multiple,
      "items": List<dynamic>.from(
        items.map(
          (x) => List<dynamic>.from(x.map((x) => x.toJson())),
        ),
      ),
    };
  }

  toArrayToJson() {
    final _array = List();
    item.forEach((x) {
      _array.add({"id": x.id});
    });

    return {
      "id": id,
      "items": _array,
    };
  }

  @override
  String toString() {
    final array = item.map((Item element) {
      return element.label;
    });

    return array.join("| ");
  }
}

class Item {
  int id;
  String label;
  bool isPrice;
  double precio;

  Item({
    this.id,
    this.label,
    this.isPrice,
    this.precio,
  });

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      id: json["id"],
      label: json["label"],
      isPrice: json["isPrice"],
      precio: json["precio"].toDouble(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "label": label,
      "isPrice": isPrice,
      "precio": precio,
    };
  }
}
