import 'package:flutter/cupertino.dart';

class ProductItems {
  String text;
  String secondaryText;
  String image;
  ProductItems({
    @required this.text,
    @required this.secondaryText,
    @required this.image,
  });
}
