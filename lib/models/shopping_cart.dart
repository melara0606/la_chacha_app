import 'dart:math';

import 'package:la_chacha_app/models/negocio.dart';
import 'package:la_chacha_app/models/product.dart';
import 'package:la_chacha_app/models/product_characteristic.dart';

class ShoppingCart {
  double pagar;
  Negocio _negocio;
  List<ItemShoppingCart> _list = List();

  ShoppingCart();

  Negocio get negocio => this._negocio;
  set negocio(Negocio item) {
    if (this._negocio != null && this._negocio.id != item.id) {
      throw new Error();
    }

    if (this._negocio == null) {
      this._negocio = item;
    }
  }

  setNegocioForze(Negocio item, ItemShoppingCart element) {
    this._negocio = item;
    this._list.clear();
    this._list.add(element);
  }

  List<ItemShoppingCart> get list => this._list;
  set list(List<ItemShoppingCart> list) {
    this._list = list;
  }

  double total() {
    double _total = 0;
    this._list.forEach((ItemShoppingCart item) {
      _total += (item.cantida * item.total);
    });
    this.pagar = _total;
    return _total;
  }

  // Delete items list
  listItemDelete(ItemShoppingCart item) {
    this._list.removeWhere((x) => x.idNumberRandom == item.idNumberRandom);
  }

  toJson() {
    final _products = List();
    this._list.asMap().values.forEach((ItemShoppingCart i) {
      final _listOpcion = List();

      i.characteristics.asMap().values.forEach((ProductCharacteristic f) {
        f.item.forEach((Item i) {
          _listOpcion.add({"id": i.id});
        });
      });

      _products.add({
        "total": i.total,
        "id": i.product.id,
        "items": _listOpcion,
        "cantidad": i.cantida,
      });
    });

    return {
      "pagar": this.pagar,
      "negocio_id": negocio.id,
      "products": _products,
    };
  }

  toArray() {
    return {
      "id": negocio.id,
      "pagar": pagar,
      "items": List<dynamic>.from(
        _list.map(
          (x) => x.toArrayToJson(),
        ),
      )
    };
  }
}

class ItemShoppingCart {
  double total;
  int cantida;
  double idNumberRandom;
  final Product product;
  List<ProductCharacteristic> characteristics = List();

  ItemShoppingCart({this.product}) {
    this.cantida = 1;
    this.total = this.product.precio;
    this.idNumberRandom =
        Random(DateTime.now().millisecondsSinceEpoch).nextDouble();
  }

  toArrayToJson() {
    return {
      "id": product.id,
      "cantidad": cantida,
      "total": total,
      "items": List<dynamic>.from(
        characteristics.map(
          (x) => List<dynamic>.from(
            x.toArrayToJson(),
          ),
        ),
      ),
    };
  }

  addMultiple(ProductCharacteristic element, Item item, bool checkout) {
    _add(element, item, (index) {
      if (checkout) {
        this.characteristics[index].item.add(item);
        this._setPrice(price: item.precio, method: 'add');
      } else {
        this
            .characteristics[index]
            .item
            .removeWhere((Item _element) => _element.id == item.id);

        this._setPrice(price: item.precio.toDouble(), method: 'reset');
        if (this.characteristics[index].item.length == 0) {
          this.characteristics.removeAt(index);
        }
      }
    });
  }

  add(ProductCharacteristic element, Item item) {
    _add(element, item, (index) {
      this.characteristics[index].item[0] = item;
      this._setPrice(price: item.precio, method: 'add');
    });
  }

  _add(ProductCharacteristic element, Item item, Function resultFuction) {
    final result = this
        .characteristics
        .indexWhere((ProductCharacteristic _e) => _e.id == element.id);

    if (result != -1) {
      if (!element.multiple) {
        Item _current = this.characteristics[result].item.elementAt(0);
        if (_current.isPrice) {
          this._setPrice(price: _current.precio.toDouble(), method: 'reset');
        }
      }
      resultFuction(result);
    } else {
      ProductCharacteristic _temporal = _generateProduct(element);
      _temporal.item.add(item);
      this.characteristics.add(_temporal);
      this._setPrice(price: item.precio, method: 'add');
    }
  }

  _generateProduct(ProductCharacteristic _e) {
    final _o = ProductCharacteristic(
      id: _e.id,
      label: _e.label,
      multiple: _e.multiple,
      items: null,
    );

    _o.item = List<Item>();
    return _o;
  }

  _setPrice({double price, String method = 'add'}) {
    method == 'add' ? this.total += price : this.total -= price;
  }

  @override
  String toString() {
    final _filter = this
        .characteristics
        .where((ProductCharacteristic item) => item.multiple == false);

    if (_filter.length > 0) {
      final _array = _filter.map((ProductCharacteristic item) {
        return item.toString();
      });
      return _array.join("| ");
    }

    return "(No hay opciones)";
  }
}
