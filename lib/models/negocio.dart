import 'dart:convert';

List<Negocio> negocioFromJson(String str) =>
    List<Negocio>.from(json.decode(str).map((x) => Negocio.fromJson(x)));

String negocioToJson(List<Negocio> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

dynamic decode(String str) {
  final sDecode = json.decode(str);
  return [
    List<Negocio>.from(
      sDecode['negocios'].map((x) => Negocio.fromJson(x)),
    ),
    List<Negocio>.from(
      sDecode['ultimate'].map((x) => Negocio.fromJson(x)),
    ),
  ];
}

class Negocio {
  int id;
  int categoriaId;
  String nombre;
  String horarioInicio;
  String horaFin;
  String minimo;
  int tipoNegocio;
  String urlToImage;
  String descripcion;

  Negocio({
    this.id,
    this.categoriaId,
    this.nombre,
    this.horarioInicio,
    this.horaFin,
    this.minimo,
    this.tipoNegocio,
    this.urlToImage,
    this.descripcion,
  });

  factory Negocio.fromJson(Map<String, dynamic> json) {
    return Negocio(
      id: json["id"],
      categoriaId: json["categoria_id"],
      nombre: json["nombre"],
      horarioInicio: json["horario_inicio"],
      horaFin: json["hora_fin"],
      minimo: json["minimo"].toString(),
      tipoNegocio: json["tipo_negocio"],
      urlToImage: json["urlToImage"] == null ? null : json["urlToImage"],
      descripcion: json['descripcion'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "categoria_id": categoriaId,
      "nombre": nombre,
      "horario_inicio": horarioInicio,
      "hora_fin": horaFin,
      "minimo": minimo,
      "tipo_negocio": tipoNegocio,
      "urlToImage": urlToImage == null ? null : urlToImage,
      "descripcion": descripcion,
    };
  }

  hora() {
    final sFinal = this.horaFin.length;
    final sInicio = this.horarioInicio.length;
    return "${this.horarioInicio.substring(0, sInicio - 3)} AM/${this.horaFin.substring(0, sFinal - 3)} PM";
  }
}
