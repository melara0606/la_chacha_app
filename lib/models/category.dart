import 'dart:convert';

List<Category> categoryFromJson(String str) =>
    List<Category>.from(json.decode(str).map((x) => Category.fromJson(x)));

String categoryToJson(List<Category> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Category {
  int id;
  String nombreCategoria;
  String urlToImage;

  Category({
    this.id,
    this.nombreCategoria,
    this.urlToImage = '',
  });

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      id: json["id"],
      nombreCategoria: json["nombre_categoria"],
      urlToImage: json["urlToImage"] == null ? null : json["urlToImage"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "nombre_categoria": nombreCategoria,
      "urlToImage": urlToImage == null ? null : urlToImage,
    };
  }
}
