import 'package:flutter/material.dart';

class UserService extends ChangeNotifier {
  String _token;
  get token => _token;
  set token(String value) {
    _token = value;
    notifyListeners();
  }
}
