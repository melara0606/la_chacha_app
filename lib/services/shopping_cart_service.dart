import 'package:flutter/material.dart';
import 'package:la_chacha_app/models/shopping_cart.dart';

class ShoppingCartService extends ChangeNotifier {
  bool _isClear;
  bool _pagar;
  ShoppingCart shoppingCart;

  ShoppingCartService() {
    this.shoppingCart = ShoppingCart();
  }

  get isClear => this._isClear;
  set isClear(bool value) {
    this._isClear = value;
    this.shoppingCart = ShoppingCart();
  }

  set pagar(bool isValid) {
    if (isValid) {
      this.shoppingCart.pagar = this.shoppingCart.total();
      notifyListeners();
    }
  }
}
