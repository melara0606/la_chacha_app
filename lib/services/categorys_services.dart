import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:la_chacha_app/api/auth_api.dart';
import 'package:la_chacha_app/api/app_confi.dart';
import 'package:la_chacha_app/models/negocio.dart';
import 'package:la_chacha_app/models/product.dart';

class CategorysService extends ChangeNotifier {
  bool _isLoading;
  bool _negocioLoading;
  Negocio _negocioSelected;
  AuthAPI _api = AuthAPI();
  List<Negocio> _listNegocios = [];
  List<Product> _listProducts = [];
  List<Negocio> _listNegociosUltimate = [];

  CategorysService() {
    getNegociosAll();
  }

  // gets and sets
  get isLoading => this._isLoading;
  get listNegocios => this._listNegocios;
  get listProducts => this._listProducts;
  get negocioLoading => this._negocioLoading;
  Negocio get negocioSelected => this._negocioSelected;
  List<Negocio> get listNegociosUltimate => this._listNegociosUltimate;

  set negocioSelected(Negocio value) {
    this._negocioSelected = value;
    this.getNegocioProductos();
    notifyListeners();
  }

  getNegocioProductos() async {
    try {
      this._listProducts = [];
      this._negocioLoading = true;
      final _id = this._negocioSelected.id;
      final _token = await this._api.getAccessToken();
      final url = "${AppConfig.apiHost}/categories/negocio/$_id/products";
      final response = await http.get(url, headers: {
        'token': _token,
      });
      final toJson = productFromJson(response.body);
      this._listProducts = toJson;
    } catch (e) {
      this._listProducts = [];
    }
    this._negocioLoading = false;
    notifyListeners();
  }

  // Methods
  getNegociosAll() async {
    this._isLoading = true;
    final _token = await this._api.getAccessToken();
    final url = "${AppConfig.apiHost}/categories/negocios";
    final response = await http.get(url, headers: {
      'token': _token,
    });
    final toJson = decode(response.body);
    this._listNegocios = toJson[0];
    this._listNegociosUltimate = toJson[1];
    this._isLoading = false;
    notifyListeners();
  }
}
