import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:la_chacha_app/api/app_confi.dart';
import 'package:la_chacha_app/api/auth_api.dart';
import 'package:la_chacha_app/models/product.dart';
import 'package:la_chacha_app/models/product_characteristic.dart';

class ProductsService extends ChangeNotifier {
  Product _product;
  bool _isLoading;
  AuthAPI _api = new AuthAPI();
  List<ProductCharacteristic> _list = [];

  Product get product => this._product;
  bool get isLoading => this._isLoading;
  List<ProductCharacteristic> get list => this._list;

  set product(Product value) {
    this._product = value;
    this.getProductItem();
    notifyListeners();
  }

  getProductItem() async {
    this._list = [];
    this._isLoading = true;
    final _id = this._product.id;
    final _token = await this._api.getAccessToken();
    final url = "${AppConfig.apiHost}/categories/products/$_id";
    final response = await http.get(url, headers: {
      'token': _token,
    });
    final toJson = productCharacteristicFromJson(response.body);
    this._list = toJson;
    this._isLoading = false;
    notifyListeners();
  }
}
