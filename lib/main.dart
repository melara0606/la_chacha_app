import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:la_chacha_app/screens/main/main.dart';
import 'package:la_chacha_app/screens/login_page.dart';
import 'package:la_chacha_app/screens/splash_page.dart';

import 'package:la_chacha_app/services/user_services.dart';
import 'package:la_chacha_app/services/products_service.dart';
import 'package:la_chacha_app/services/categorys_services.dart';
import 'package:la_chacha_app/services/shopping_cart_service.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserService()),
        ChangeNotifierProvider(create: (_) => ProductsService()),
        ChangeNotifierProvider(create: (_) => CategorysService()),
        ChangeNotifierProvider(create: (_) => ShoppingCartService()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'LaChachaApp',
        home: SplashPage(),
        routes: {
          'splash': (_) => SplashPage(),
          'main': (_) => MainPage(),
          'login': (_) => LoginPage(),
        },
      ),
    );
  }
}
