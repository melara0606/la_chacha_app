import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class Dialogs {
  static void alert(BuildContext context,
      {String title = '', String message = ''}) {
    showDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(title,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            content: Text(message,
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300)),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Ok'),
              )
            ],
          );
        });
  }

  static void alertShoppingCart(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(
              "Exito",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            content: Text(
              "Hemos procesado su pedido, en momentos nos podremos en contacto con usted",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pushReplacementNamed(context, "main");
                },
                child: Text('Ok'),
              )
            ],
          );
        });
  }

  static confirm(
    BuildContext context, {
    String title = '',
    String message = '',
    VoidCallback onCancel,
    VoidCallback onConfirm,
  }) {
    return showDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(
            title,
            style: TextStyle(
              fontSize: 20,
              height: 1.2,
              fontFamily: "Montserrat",
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text(
            message,
            style: TextStyle(
              height: 1.2,
              fontSize: 18,
              fontFamily: "NunitoSans",
              fontWeight: FontWeight.w300,
            ),
          ),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: onConfirm,
              child: Text(
                'Ok',
                style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: "Montserrat",
                ),
              ),
            ),
            CupertinoDialogAction(
              onPressed: onCancel,
              child: Text(
                'Cancelar',
                style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: "Montserrat",
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
