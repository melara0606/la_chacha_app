import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class Utils {
  static inputMarks() {
    return MaskTextInputFormatter(
      mask: '(+503) ####-####',
      filter: {'#': RegExp(r'[0-9]')},
    );
  }
}
